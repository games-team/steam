Source: steam
Section: non-free/games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Michael Gilbert <mgilbert@debian.org>,
 Simon McVittie <smcv@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 libexpat1,
 libffi-dev,
 libgcc-s1 | libgcc1,
 libstdc++6,
 libtinfo-dev,
 libx11-6,
 libx11-xcb1,
 libxau6,
 libxcb-dri2-0,
 libxcb-dri3-0,
 libxcb-glx0,
 libxcb-present0,
 libxcb-sync1,
 libxcb1,
 libxdamage1,
 libxdmcp6,
 libxext6,
 libxfixes3,
 libxi6,
 libxxf86vm1,
 zlib1g,
 python3:any,
Rules-Requires-Root: no
XS-Autobuild: yes
Standards-Version: 4.6.1
Homepage: https://steamcommunity.com/linux
Vcs-Git: https://salsa.debian.org/games-team/steam.git
Vcs-Browser: https://salsa.debian.org/games-team/steam

Package: steam
Architecture: i386
Multi-Arch: foreign
Pre-Depends:
 debconf,
 ${misc:Pre-Depends},
Depends:
 curl,
 file,
 libcrypt1 | libc6 (<< 2.29-4),
 libgcc-s1 | libgcc1,
 libgl1-mesa-dri (>= 17.3),
 libgl1,
 libgpg-error0 (>= 1.10),
 libstdc++6,
 libudev1,
 libva2,
 libva-x11-2,
 libxcb-dri3-0 (>= 1.11.1),
 libxcb1,
 libxi6 (>= 2:1.7.4),
 libxinerama1 (>= 2:1.1.1),
 xz-utils,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ca-certificates,
 fontconfig,
 fonts-liberation,
 libasound2-plugins,
 libegl1,
 libfontconfig1,
 libgbm1,
 libnm0,
 libsdl2-2.0-0,
 libva-drm2,
 libva-glx2,
 libxss1,
 mesa-vulkan-drivers,
 steam-devices,
 va-driver-all | va-driver,
 xdg-desktop-portal,
 xdg-desktop-portal-gtk | xdg-desktop-portal-backend,
 xdg-utils,
 xterm | x-terminal-emulator,
 zenity,
Suggests:
 nvidia-driver-libs,
 nvidia-vulkan-icd,
 pipewire,
Conflicts:
 steam-launcher,
Replaces:
 steam-launcher,
Description: Valve's Steam digital software delivery system
 Steam (https://www.steampowered.com) is a software content delivery system
 developed by Valve software (https://www.valvesoftware.com).  There is
 some free software available, but for the most part the content delivered
 is non-free.
 .
 This package comes with a fairly substantial non-free license agreement
 that must be accepted before installing the software.  If you have any
 opposition to non-free work, please select "I DECLINE" during the package
 installation dialogs.   There are also additional agreements in various
 parts of the application that differ from the original agreement.  In
 other words, pay attention and read carefully if you're worried about your
 rights.

Package: steam-devices
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Recommends:
 steam,
Breaks:
 steam (<< ${source:Version}),
 steam (>= ${source:Version}.),
Conflicts:
 steam-launcher,
Replaces:
 steam (<< ${source:Version}),
 steam (>= ${source:Version}.),
 steam-launcher,
Description: Device support for Steam-related hardware
 This package provides udev rules for various Steam-related hardware devices
 such as the Steam Controller (gamepad) and the HTC Vive (virtual reality
 headset).
